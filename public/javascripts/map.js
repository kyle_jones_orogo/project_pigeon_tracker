function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 51.5, lng: 0.12},
          zoom: 10,
          styles: [{
            featureType: 'poi',
            stylers: [{ visibility: 'off' }]  // Turn off points of interest.
          }, {
            featureType: 'transit.station',
            stylers: [{ visibility: 'off' }]  // Turn off bus stations, train stations, etc.
          }],
          disableDoubleClickZoom: true
        });
    }

const firebaseRoot = "https://pigeonproject-9b18a.firebaseio.com",
    ordersRef = "/orders/",
    orderHistoryRef = "/orderHistory/";
// Initialize Firebase
const firebaseConfig = {
    apiKey: "AIzaSyDo51YnIRa6Ppt4WaznJ43CBBWRCV5uzRQ",
    authDomain: "pigeonproject-9b18a.firebaseapp.com",
    databaseURL: firebaseRoot,
    storageBucket: "pigeonproject-9b18a.appspot.com",
};    
/*
const firebaseConfig = {
    apiKey: "AIzaSyDCBHCVVz5ecqds5Wvr5xWuuoGqGFT8SSE",
    authDomain: "orogo-2014.firebaseapp.com",
    databaseURL: "https://orogo-2014.firebaseio.com",
    storageBucket: "orogo-2014.appspot.com",
};
*/

firebase.initializeApp(firebaseConfig);
var fb = firebase.database().ref();
var orderID = 2;
var map;
//Watch on firebase for changes on the customer
fb.child(orderHistoryRef + orderID + '/driver').on('value', function(snapshot) {
    var locations = snapshot.val();
    for (var location in locations) {
        var position = {
            lat: locations[location].latitude, 
            lng: locations[location].longitude
        };
        var marker = new google.maps.Marker({
            position: position,
            map: map,
            title: locations[location].timestamp
        }); 
    }
    map.setCenter(new LatLng(locations[locations.length-1].latitude, locations[locations.length-1].longitude));
});